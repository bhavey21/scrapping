const axios = require('axios');
const cheerio = require("cheerio");
const fs = require('fs')


const scrapping = async (keyword)=>{
    const  {data}  = await axios.get('https://www.google.com/search?q='+keyword, {
        headers: {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36",
        },
    });
    let $ = cheerio.load(data);

    const links = [];
   
    $(".yuRUbf > a").each((i, el) => {
      links[i] = $(el).attr("href");
    });
   
   return links
}


    fs.readFile('./keywords.txt', 'utf8' ,async (err, data) => {
        if (err) {
          console.error(err)
          return
        }
      
      
        const link = await Promise.all(
            (data.split('\n')).map(async(ele)=>{
                //return ele.trim();
           
                   const data = await scrapping(ele.trim())
                   return JSON.stringify({element:ele,links:data});
             })
        )
        console.log(link)
        fs.writeFile('./links.txt', link.toString(), err => {
            if (err) {
              console.error(err)
              return
            }
          })
      })

//scrapping();